package FactoryModel.Storage;

import FactoryModel.Auto.Component.Component;
import FactoryModel.Auto.Component.Motor;
import FactoryModel.FactoryModel;
import FactoryModel.Interfaces.Notifier;
import FactoryModel.Interfaces.SupplierInterface;
import FactoryModel.Interfaces.StorageInterface;

import java.util.ArrayList;

public class MotorStorage implements StorageInterface, Notifier {
    SupplierInterface motorSupplier;
    ArrayList<Motor> storage;
    FactoryModel listener;
    int capacity;
    int motorCount;
    int motorCountAll;

    public MotorStorage(int capacity, SupplierInterface motorSupplier, FactoryModel listener) {
        this.motorSupplier = motorSupplier;
        this.motorSupplier.addStorage(this);
        this.storage = new ArrayList<>();
        this.capacity = capacity;
        this.motorCount = 0;
        this.motorCountAll = 0;
        this.listener = listener;
    }

    @Override
    synchronized public void addComponent(Component component) throws InterruptedException {
        while (motorCount >= capacity) {
            wait();
        }
        motorCountAll++;
        motorCount++;
        listener.update(this);
        notifyAll();
        storage.add((Motor)component);
    }

    @Override
    synchronized public Component getComponent() throws InterruptedException {
        while (motorCount <= 0) {
            wait();
        }
        motorCount--;
        listener.update(this);
        notifyAll();
        Motor motor = storage.get(storage.size() - 1);
        storage.remove(storage.size() - 1);
        return motor;
    }

    @Override
    public int getComponentCount(){
        return motorCount;
    }

    @Override
    public long getComponentCountAllTime(){
        return motorCountAll;
    }

}
