package FactoryModel;

import FactoryModel.Dealer.*;
import FactoryModel.Interfaces.Notifier;
import FactoryModel.Interfaces.SupplierInterface;
import FactoryModel.Supplier.AccessorySupplier.AccessorySuppliersPool;
import FactoryModel.Supplier.BodySupplier;
import FactoryModel.Supplier.MotorSupplier;
import FactoryView.FactoryView;
import FactoryModel.Storage.AccessoryStorage;
import FactoryModel.Storage.AutoStorage.AutoStorage;
import FactoryModel.Storage.AutoStorage.AutoStorageController;
import FactoryModel.Storage.BodyStorage;
import FactoryModel.Storage.MotorStorage;
import FactoryModel.ThreadPool.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class FactoryModel {
    static HashMap<String, Integer> correspondenceMap = new HashMap<>();
    FactoryView view;
    AccessorySuppliersPool accessorySuppliersPool;
    BodySupplier bodySupplier;
    MotorSupplier motorSupplier;
    Thread bodyThread;
    Thread motorThread;
    AccessoryStorage accessoryStorage;
    BodyStorage bodyStorage;
    MotorStorage motorStorage;
    AutoStorage autoStorage;
    ThreadPool threadPool;
    AutoStorageController autoStorageController;
    DealerPool dealerPool;
    boolean logSale;

    public void addObserver(FactoryView view) {this.view = view; }

    public void startFactory() {
        try {
            File file = new File("factoryConfig");
            FileReader fr = new FileReader(file);
            Scanner scan = new Scanner(fr);
            while(scan.hasNextLine()){
                String[] attrAndValue = scan.nextLine().split("=");
                if (attrAndValue.length != 2) {
                    throw new IOException("too many args");
                }
                if(!attrAndValue[0].contains("LogSale")) {
                    correspondenceMap.put(attrAndValue[0], Integer.parseInt(attrAndValue[1]));
                }else{
                    logSale = Boolean.parseBoolean(attrAndValue[1]);
                }
            }
            scan.close();
            createThreads();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void update(Notifier notifier) {
        view.update(notifier);
    }

    private void createThreads() {
        bodySupplier = new BodySupplier();
        motorSupplier = new MotorSupplier();
        bodyThread  = new Thread(bodySupplier, "bodyThread");
        motorThread = new Thread(motorSupplier, "motorThread");
        accessorySuppliersPool = new AccessorySuppliersPool(correspondenceMap.get("AccessorySuppliers"));
        bodyStorage = new BodyStorage(correspondenceMap.get("StorageBodySize"), bodySupplier, this);
        motorStorage = new MotorStorage( correspondenceMap.get("StorageMotorSize"), motorSupplier, this);
        accessoryStorage = new AccessoryStorage(correspondenceMap.get("StorageAccessorySize"), accessorySuppliersPool.getAccessoryProviders(), this);
        autoStorage = new AutoStorage(correspondenceMap.get("StorageAutoSize"), this, logSale);
        threadPool = new ThreadPool(correspondenceMap.get("Workers"), this);
        autoStorageController = new AutoStorageController(autoStorage, threadPool);
        dealerPool = new DealerPool( correspondenceMap.get("Dealers"), autoStorage);

        Task.setBodyStorage(bodyStorage);
        Task.setAccessoryStorage(accessoryStorage);
        Task.setMotorStorage(motorStorage);
        Task.setAutoStorage(autoStorage);

        motorThread.start();
        bodyThread.start();
        accessorySuppliersPool.startProviders();
        autoStorageController.start();
        dealerPool.startDealers();
    }

    public void setBodyProdSpeed(int prodSpeed) {
        bodySupplier.setProdSpeed(prodSpeed);
    }

    public long getBodyCountAllTime() {
        return bodyStorage.getComponentCountAllTime();
    }

    public int getBodyCountNow() {
        return bodyStorage.getComponentCount();
    }

    public void setAccessoryProdSpeed(int prodSpeed) {
        ArrayList<SupplierInterface> suppliers = accessorySuppliersPool.getAccessoryProviders();

        for (SupplierInterface supplier : suppliers) {
            supplier.setProdSpeed(prodSpeed);
        }
    }

    public long getAccessoryCountAllTime() {
        return accessoryStorage.getComponentCountAllTime();
    }

    public int getAccessoryCountNow() {
        return accessoryStorage.getComponentCount();
    }

    public void setMotorProdSpeed(int prodSpeed) {
        motorSupplier.setProdSpeed(prodSpeed);
    }

    public long getMotorCountAllTime(){
        return motorStorage.getComponentCountAllTime();
    }

    public int getMotorCountNow(){
        return motorStorage.getComponentCount();
    }

    public void setAutoProdSpeed(int prodSpeed) {
        ArrayList<Dealer> dealers = dealerPool.getDealers();

        for (Dealer dealer : dealers) {
            dealer.setProdSpeed(prodSpeed);
        }
    }

    public long getAutoCountAllTime() {
        return autoStorage.getAutoCountAll();
    }

    public int getAutoCountNow() {
        return autoStorage.getAutoCount();
    }

    public int getQueueCount() {
        return threadPool.inQueueCount();
    }

    public void close() {
        bodyThread.interrupt();
        motorThread.interrupt();
        accessorySuppliersPool.close();
        dealerPool.close();
        autoStorageController.close();
        threadPool.close();
        autoStorage.closeFile();
    }
}
